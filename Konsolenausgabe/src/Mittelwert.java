
public class Mittelwert {

	public static void main(String[] args) {
		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
		// ===========================
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
		int anzahl = tastatur.nextInt();
		double summe = 0;
		int zaehlschleife = 0;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		while (zaehlschleife < anzahl) {
			System.out.println("Bitte gebe eine Zahl f�r die Mittelwertberechnung ein: ");
			double zahl = tastatur.nextDouble();
			summe = summe + zahl;
			zaehlschleife++;
		}
		double mittelwert = summe /anzahl;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.print("Der Mittelwert lautet: " + mittelwert);


	}

}
