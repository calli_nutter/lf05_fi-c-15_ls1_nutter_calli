import java.util.Scanner;
public class Steuersatz {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie den Nettowert ein welcher versteuert werden soll:");
		float nettowert = scan.nextFloat();
		
		System.out.println("Geben Sie nun den gew�nschten Steuersatz an. Dies k�nnen sie mit den Buchstaben n und j machen.");
		System.out.println("n = voller Steuersatz (19%)");
		System.out.println("j = erm��igter Steuersatz (7%)");
		char steuersatz = scan.next().toCharArray()[0];
		
		if ( steuersatz == 'n' ) {
			System.out.println("Sie haben den vollen Steuersatz gew�hlt. Somit entspricht der Bruttobetrag " + bruttobetragVollerSteuersatz(nettowert) + "�.");
		} else if ( steuersatz == 'j' ) {
			System.out.println("Sie haben den erm��igten Steuersatz gew�hlt. Somit entspricht der Bruttobetrag " + bruttobetragErmae�igterSteuersatz(nettowert) + "�.");
		}
		
		
		

	}
	
	public static float bruttobetragVollerSteuersatz(float nettowert) {
		
		float bruttobetragVollerSteuersatz;
		
		bruttobetragVollerSteuersatz = (nettowert * 0.19f) + nettowert;

		return bruttobetragVollerSteuersatz;
	}
	
	public static float bruttobetragErmae�igterSteuersatz(float nettowert) {
		
		float bruttobetragErmae�igterSteuersatz;
		
		bruttobetragErmae�igterSteuersatz = (nettowert * 0.07f) + nettowert;

		return bruttobetragErmae�igterSteuersatz;
	}
	
}
