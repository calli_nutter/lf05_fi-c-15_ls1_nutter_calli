import java.util.Scanner;


public class Auswahlstrukturen {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl X ein:");
		int x = scan.nextInt();
		System.out.println("Geben Sie eine Zahl Y ein:");
		int y = scan.nextInt();
		System.out.println("Geben Sie eine Zahl Z ein:");
		int z = scan.nextInt();
		
		Aufgabe2(x, y);
		// Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If)

		Aufgabe3(x, y);
		// Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If)
		
		Aufgabe4(x, y);
		// Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben
		// werden, ansonsten eine andere Meldung (If-Else)
		
	    Aufgabe5(x, y, z);		
	    //Wenn die 1.Zahl größer als die 2.Zahl und die 3. Zahl ist, soll eine Meldung
	    //ausgegeben werden (If mit && / Und)
	    
	    Aufgabe6(x, y, z);
	    //Wenn die 3.Zahl größer als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung
	    //ausgegeben werden (If mit || / Oder)
	    
	    Aufgabe7(x,y,z);
	    //Geben Sie die größte der 3 Zahlen aus. (If-Else mit && / Und)

		
	}
	
	public static void Aufgabe2(int x, int y) {
		if ( x == y ) {
			System.out.println("Der Wert X (" + x + ") und Y (" + y + ") sind gleich.");
		}
	}
	
	public static void Aufgabe3(int x, int y) {
		if ( y > x ) {
			System.out.println("Der Wert Y (" + y + ") ist groesser als X (" + x + ").");
		}
	}
	
	public static void Aufgabe4(int x, int y) {
		if ( x > y ) {
			System.out.println("Der Wert X (" + x + ") ist groesser als Y (" + y + ").");
		} else if (x == y) {
			System.out.println("Der Wert X (" + x + ") und Y (" + y + ") sind gleich.");
		} else {
			System.out.println("Die Werte X (" + x +") ist weder gleich noch groesser als Y (" + y + ").");
		}
	}
	
	public static void Aufgabe5(int x, int y, int z) {
		if(x > 2 && x > z) {
			System.out.println("success");
		}else {
			System.out.println("unsuccessful");
		}
	}
	
	public static void Aufgabe6(int x, int y, int z) {
		if(z > y || z > x) {
			System.out.println("success");
		}else {
			System.out.println("unsuccessful");
		}
	}
	public static void Aufgabe7(int x, int y, int z) {
		if(x > y && x > z) {
			System.out.println(x + "X ist die grösten Zahl");
		} else if(y > x && y > z) {
			System.out.println(y  + "Y ist die grösten Zahl");
		} else if(z > x && z > y) {
			System.out.println(z  + "Z ist die grösten Zahl");
		}
	}
}
