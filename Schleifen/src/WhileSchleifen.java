import java.util.Scanner;

public class WhileSchleifen {
	static Scanner tastatur = new Scanner(System.in);
	public static void main(String[] args) {
		// A4.4 Aufgabe 2: Summe
		System.out.println("Geben Sie einen Nummer ein, um die Summierung zu begrenzen.");
		int zahl = tastatur.nextInt();
		//a)
		int i=1;
		int n=1;
		int c=1;
		int summe = 0;
		int summeb = 1;
		int summec = 1;
		while(i<=zahl) {
			summe = summe + i;
			i++;
		}
		
		while(n<=zahl) {
			summeb = summeb + n * 2;
			n++;
		}
		
		while(c<zahl) {
			summec = summec + (c * 2 + 1);
			c++;
		}
		
		System.out.println("Die Summe f�r A betr�gt: " + summe);
		System.out.println("Die Summe f�r B betr�gt: " + summeb);
		System.out.println("Die Summe f�r C betr�gt: " + summec);

	}

}
