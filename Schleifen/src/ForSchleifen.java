import java.util.Scanner;

public class ForSchleifen {

	public static void main(String[] args) {
		// A4.4 Aufgabe 1: Z�hlen
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie einen Nummer ein.");
		int zahl = tastatur.nextInt();
		//a)
		for(int i=1;i<=zahl;i++) {
			System.out.println(i);
		}
		//b)
		System.out.println("kehren wir es um");
		for(int i=zahl;i>0;i--) {
			System.out.println(i);
		}

	}
}
